import { defineStore } from 'pinia';
import { uid } from 'uid';
import { isObj, isNonEmptyArr } from '@/utils';
import { users as initialState } from '@/stores/initialState';
import { users as persist } from '@/stores/persistedState';
import { userRoles } from '@/configs/roles';

import { users as mockedUsersList } from '@/mocks/users';

/**
 * @description Is valid
 * @param users
 * @returns {boolean}
 */
const isValid = (users) => isObj(users)
  && !isNonEmptyArr(users.items);

/**
 * @description Init state
 * @param initialState
 * @returns {*}
 */
const initState = (initialState) => {
  if (!isValid(initialState)) {
    throw Error('Invalid initial users store state');
  }

  const { items } = initialState;
  return () => ({
    items,
  });
};

const getters = {
  users: ({ items }) => items,
};

const actions = {
  setMockedUsers() {
    const nextMockedUserList = [
      ...mockedUsersList.map(({ id, ...rest }) => ({
        ...rest,
        id: uid(),
        role: userRoles.find(({ id }) => id === rest.roleId).label,
      }))
    ];

    this.items = nextMockedUserList;
  },
};

export const userStore = defineStore('user', {
  state: initState(initialState),
  getters,
  actions,
  persist,
});

export default userStore;
