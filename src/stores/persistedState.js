export const auth = { paths: ['id', 'firstName', 'lastName', 'email', 'phone', 'roleId', 'permissions'] };
export const users = { paths: ['items'] };

export default {
  auth,
  users,
};
