export const auth = {
  id: null,
  firstName: null,
  lastName: null,
  email: null,
  phone: null,
  roleId: null,
  permissions: [],
};

export const users = {
  items: [],
};

export default {
  auth,
  users,
};
