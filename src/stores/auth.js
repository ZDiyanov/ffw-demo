import { toRaw } from 'vue';
import { defineStore } from 'pinia';
import { ability } from '@/plugins/casl';
import { isObj, isNull, isNonEmptyArr, isStr } from '@/utils';
import { isAuthorized } from '@/utils/permissions';
import { auth as initialState } from '@/stores/initialState';
import { auth as persist } from '@/stores/persistedState';

/**
 * @description Is valid
 * @param auth
 * @returns {boolean}
 */
const isValid = (auth) => isObj(auth)
  && isNull(auth.id)
  && isNull(auth.firstName)
  && isNull(auth.lastName)
  && isNull(auth.email)
  && isNull(auth.phone)
  && isNull(auth.roleId)
  && !isNonEmptyArr(auth.permissions);

/**
 * @description Init state
 * @param initialState
 * @returns {*}
 */
const initState = (initialState) => {
  if (!isValid(initialState)) {
    throw Error('Invalid initial auth store state');
  }

  const { id, firstName, lastName, email, phone, roleId, permissions } = initialState;
  return () => ({
    id,
    firstName,
    lastName,
    email,
    phone,
    roleId,
    permissions,
  });
};

/**
* @description Set ability
* @param permissions
*/
const setAbility = (permissions) => {
  ability.update(permissions);
};

/**
* @description Revoke ability
*/
const revokeAbility = () => {
  ability.update([]);
};

const getters = {
  activeUser: ({ id, firstName, lastName, email, phone, roleId }) => (
    { id, firstName, lastName, email, phone, roleId }
  ),
  userId: ({ id }) => id,
  userName: ({ firstName, lastName }) => `${firstName} ${lastName}`,
  userEmail: ({ email }) => email,
  userRole: ({ roleId }) => roleId,
  userPhone: ({ phone }) => phone,
  userPermissions: ({ permissions }) => permissions,
  isAuthorized: ({ permissions }) => (requestedPermissions) => (
    isAuthorized(toRaw(permissions), requestedPermissions)
  ),
  isLogged: ({ id }) => isStr(id),
};

const actions = {
  login(nextUser) {
    const { id, firstName, lastName, email, phone, roleId, permissions } = nextUser;

    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.phone = phone;
    this.roleId = roleId;
    this.permissions = permissions;

    setAbility(permissions);

    this.$router.push({
      name: 'user',
      params: { id },
    });
  },
  logout() {
    const { id, firstName, lastName, email, phone, roleId, permissions } = initialState;

    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.phone = phone;
    this.roleId = roleId;
    this.permissions = permissions;

    revokeAbility();
    this.$router.push({ name: 'login' });
  }
};

export const authStore = defineStore('auth', {
  state: initState(initialState),
  getters,
  actions,
  persist,
});

export default authStore;
