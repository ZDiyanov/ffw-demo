const {
  MODE,
  VITE_SERVICES_URL,
  VITE_SERVICES_REQUEST_TIMEOUT_MS,
  VITE_SERVICES_CLIENT_ID,
  VITE_SERVICES_CLIENT_TOKEN,
  VITE_WEB_SOCKET_URL,
  VITE_ENABLE_ANALYTICS,
  VITE_ENABLE_ERROR_REPORTS,
  VITE_GOOGLE_TAG_MANAGER_ID,
  VITE_FACEBOOK_APP_ID,
  VITE_SENTRY_DSN,
} = import.meta.env;

const env = MODE;

export const isDev = env === 'development';
export const isStage = env === 'staging';
export const isProd = env === 'production';

export default {
  env,
  services: {
    requestTimout: Number(VITE_SERVICES_REQUEST_TIMEOUT_MS) || 0,
    url: VITE_SERVICES_URL,
    clientId: VITE_SERVICES_CLIENT_ID,
    clientToken: VITE_SERVICES_CLIENT_TOKEN,
    webSocketUrl: VITE_WEB_SOCKET_URL,
  },
  analytics: {
    enabled: VITE_ENABLE_ANALYTICS === 'true',
    errorReports: VITE_ENABLE_ERROR_REPORTS === 'true',
    gtm: {
      enabled: Boolean(VITE_GOOGLE_TAG_MANAGER_ID),
      id: VITE_GOOGLE_TAG_MANAGER_ID,
    },
    facebook: {
      enabled: Boolean(VITE_FACEBOOK_APP_ID),
      id: VITE_FACEBOOK_APP_ID,
    },
    sentry: {
      enabled: Boolean(VITE_SENTRY_DSN),
      dsn: VITE_SENTRY_DSN,
    },
  },
};
