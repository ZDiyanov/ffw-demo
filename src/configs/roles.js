export const patient = {
  id: 1,
  slug: 'role_patient',
  label: 'Patient',
};

export const doctor = {
  id: 2,
  slug: 'role_doctor',
  label: 'Doctor',
};

export const nurse = {
  id: 3,
  slug: 'role_nurse',
  label: 'Nurse',
};

export const userRoles = [patient, doctor, nurse];
export default userRoles;
