export const users = [
  {
    id: null,
    firstName: 'Danniel',
    lastName: 'Blichman',
    email: 'danniel.blichman@testtask.com',
    phone: '(XXX)-XXX-XXXX',
    roleId: 2,
    permissions: [],
  },
  {
    id: null,
    firstName: 'John',
    lastName: 'Appleseed',
    email: 'john.appleseed@testtask.com',
    phone: '(XXX)-XXX-XXXX',
    roleId: 2,
    permissions: [],
  },
  {
    id: null,
    firstName: 'Margarrete',
    lastName: 'Jones',
    email: 'margarrete.jones@testtask.com',
    phone: '(XXX)-XXX-XXXX',
    roleId: 3,
    permissions: [],
  },
  {
    id: null,
    firstName: 'Bethany',
    lastName: 'Doe',
    email: 'bethany.doe@testtask.com',
    phone: '(XXX)-XXX-XXXX',
    roleId: 1,
    permissions: [
      { action: 'update', subject: 'user' },
    ],
  },
  {
    id: '',
    firstName: 'Samuel',
    lastName: 'Jackson',
    email: 'samuel.jackson@testtask.com',
    phone: '(XXX)-XXX-XXXX',
    roleId: 1,
    permissions: [
      { action: 'update', subject: 'user' },
    ],
  },
  {
    id: '',
    firstName: 'Persival',
    lastName: 'Blinn',
    email: 'persival.blinn@testtask.com',
    phone: '(XXX)-XXX-XXXX',
    roleId: 2,
    permissions: [],
  },
];

export default { users };
