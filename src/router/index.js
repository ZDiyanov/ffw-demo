import { createRouter, createWebHistory } from 'vue-router';
import { basic } from '@/router/routes';

const config = {
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [...basic],
};

export const router = createRouter(config);

export default router;
