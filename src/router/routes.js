import LoginView from '@/views/Login';
import UserView from '@/views/User';

export const basic = [
  {
    path: '/login',
    name: 'login',
    meta: {
      slug: 'view_login',
      auth: false,
      isAuthView: true,
    },
    component: LoginView,
  },
  {
    path: '/',
    name: 'index',
    meta: {
      slug: 'view_index',
      auth: true,
      isAuthView: false,
    },
    component: LoginView,
  },
  {
    path: '/user/:id',
    name: 'user',
    meta: {
      slug: 'view_user',
      auth: true,
      isAuthView: false,
      permissions: [
        { action: 'read', subject: 'user' },
      ],
    },
    component: UserView,
  },
];

export default { basic };
