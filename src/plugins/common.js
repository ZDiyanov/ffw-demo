import BaseLayout from '@/components/layouts/Basic';
import TextField from '@/components/common/TextField';
import BasicBtn from '@/components/common/BasicBtn';

export const plugin = {
  install: (app) => {
    app.component('BaseLayout', BaseLayout);
    app.component('TextField', TextField);
    app.component('BasicBtn', BasicBtn);
  },
};

export default plugin;
