export { plugin as config } from '@/plugins/config';
export { plugin as common } from '@/plugins/common';
export { plugin as head } from '@/plugins/head';
export { plugin as casl } from '@/plugins/casl';
