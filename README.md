# FFW Demo

A simple app that consists of a mocked permission system and a profile card component that relies on a permission check for behaviour.

The focus of the task was component reusability, all of the component presentation logic relies on a single prop for making the visual and functional adjustments. The component checks for a single boolean and enables/disables the phone field which can be validated before submission.

To test the app we mocked a user list and saved it on your local machine, you should select and load a single user profile. The profile object holds it's own mocked permission array, the modification condition for the component is based on the permission on all of the mocked patient profile objects.

This demo project can be used as a basis for any other testing tasks that your team may want us to implement and/or discuss.

## Project Setup

```sh
yarn install
```

### Compile and Hot-Reload for Development

```sh
yarn run dev
```

